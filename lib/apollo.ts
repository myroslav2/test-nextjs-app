import { IncomingMessage, ServerResponse } from 'http'
import { useMemo } from 'react'
import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client'

let apolloClient: ApolloClient<NormalizedCacheObject> | undefined

export type ResolverContext = {
  req?: IncomingMessage
  res?: ServerResponse
}

function createIsomorphLink(uri: string) {
  const { HttpLink } = require('@apollo/client')
  return new HttpLink({
    credentials: 'same-origin',
    uri,
    headers: {
      authorization: process.env.API_KEY,
      store_id: 1,
      user_id: 1
    },
  })
}

function createApolloClient(uri: string) {
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: createIsomorphLink(uri),
    cache: new InMemoryCache(),
  })
}

export function initializeApollo(
  initialState: any = null,
  context?: ResolverContext,
) {
  console.log(context?.req?.headers)
  console.log(context?.req?.url)
  const _apolloClient = apolloClient ?? createApolloClient(`https://${context?.req?.headers.host}/graphql`)

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    _apolloClient.cache.restore(initialState)
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}

export function useApollo(initialState: any) {
  const store = useMemo(() => initializeApollo(initialState), [initialState])
  return store
}
