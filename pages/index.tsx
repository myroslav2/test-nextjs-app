import Link from 'next/link'
import { useCountriesQuery, CountriesDocument } from '../lib/countries.graphql'
import { initializeApollo } from '../lib/apollo'
import {useEffect} from "react";

const Index = () => {
  const { countries } = useCountriesQuery().data!
    useEffect(() => {
        fetch("/about-me", { mode: 'cors', credentials: 'include' })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error)
                }
            )
    }, [])

    const makeSetCookieRequest = (e: any) => {
        e.preventDefault()
        fetch("/set-cookie", { mode: 'cors', credentials: 'include' })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error)
                }
            )
    }

  return (
    <div>
        <ul>
            {countries.items.map((country) =>
                <li>
                    {country.name}
                </li>
            )}
        </ul>
        <a href='#' onClick={makeSetCookieRequest}>set cookie</a>
        <br />
      <Link href="/about">
        <a>about</a>
      </Link>{' '}
      page.
    </div>
  )
}

export async function getServerSideProps(a: any) {
  const apolloClient = initializeApollo(null, a)

  await apolloClient.query({
    query: CountriesDocument,
  })

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  }
}

export default Index
